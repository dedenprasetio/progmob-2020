package com.example.myfirstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myfirstapp.Pertemuan2.ListActivity;
import com.example.myfirstapp.Pertemuan2.RecyclerActivity;
import com.example.myfirstapp.Pertemuan2.cardviewtestactivity;
import com.example.myfirstapp.Pertemuan4.DebuggingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //variable
        final TextView txView = (TextView)findViewById(R.id.mainActivityTextView);
        Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.editText1);
        Button btnHelp = (Button)findViewById(R.id.buttonHelp);
        Button btnTracker = (Button)findViewById(R.id.buttonTracker);


        //Pertemuan 2
        Button btnList = (Button)findViewById(R.id.buttonListView);
        Button btnRecycler = (Button)findViewById(R.id.buttonRecycleView);
        Button btnCard = (Button)findViewById(R.id.buttonCardView);

        //Pertemuan 4
        Button btnPertemuan = (Button)findViewById(R.id.buttonPertemuan);






        //action
        txView.setText(R.string.text_hello);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                //Log.d("COBA KLIKK", myEditText.getText().toString());
                txView.setText(myEditText.getText().toString());
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, HelpActivity.class);
                Bundle b = new Bundle();

                b.putString("help_string",myEditText.getText().toString());
                intent.putExtras(b);

                startActivity(intent);
            }
        });

        btnTracker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, TrackerActivity.class);
                startActivity(intent);
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });

        btnRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });

        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, cardviewtestactivity.class);
                startActivity(intent);
            }
        });

        btnPertemuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });










    }
}