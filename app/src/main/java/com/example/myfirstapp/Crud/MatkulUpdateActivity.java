package com.example.myfirstapp.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirstapp.Model.DefaultResult;
import com.example.myfirstapp.Network.GetDataService;
import com.example.myfirstapp.Network.RetrofitClientInstance;
import com.example.myfirstapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        EditText edKodelama = (EditText)findViewById(R.id.edKodeDicari);
        EditText edNamamt = (EditText)findViewById(R.id.edNamaMtBaru);
        EditText edKode = (EditText)findViewById(R.id.edKodeMtBaru);
        EditText edhari = (EditText)findViewById(R.id.edHariBaru);
        EditText edsesi = (EditText)findViewById(R.id.edSesiBaru);
        EditText edsks = (EditText)findViewById(R.id.edSksBaru);
        Button btnupdate = (Button)findViewById(R.id.buttonUpdateMatkul);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null){
            edNamamt.setText(data.getStringExtra("nama"));
            edKode.setText(data.getStringExtra("kode"));
            edhari.setText(data.getStringExtra("hari"));
            edsesi.setText(data.getStringExtra("sesi"));
            edsks.setText(data.getStringExtra("sks"));
            edKodelama.setText(data.getStringExtra("kode"));
        }

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edNamamt.getText().toString(),
                        edKode.getText().toString(),
                        edhari.getText().toString(),
                        edsesi.getText().toString(),
                        edsks.getText().toString(),
                        edKodelama.getText().toString(),
                        "72180201"
                        //ednidnlama.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MatkulUpdateActivity.this, MainMatkulActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data Tidak Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}