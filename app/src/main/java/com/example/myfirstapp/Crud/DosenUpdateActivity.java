package com.example.myfirstapp.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirstapp.Model.DefaultResult;
import com.example.myfirstapp.Network.GetDataService;
import com.example.myfirstapp.Network.RetrofitClientInstance;
import com.example.myfirstapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_update);

        EditText ednidnlama = (EditText)findViewById(R.id.edNidnDicari);
        EditText ednama = (EditText)findViewById(R.id.edNamaDsnBaru);
        EditText ednidn = (EditText)findViewById(R.id.edNidnBaru);
        EditText edalamat = (EditText)findViewById(R.id.edAlamatDsnBaru);
        EditText edemail = (EditText)findViewById(R.id.edEmailDsnBaru);
        EditText edgelar = (EditText)findViewById(R.id.edGelarDsnBaru);
        Button btnedit = (Button)findViewById(R.id.buttonUpdateDsn);
        pd = new ProgressDialog(DosenUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null){
            ednama.setText(data.getStringExtra("nama"));
            ednidn.setText(data.getStringExtra("nidn"));
            edalamat.setText(data.getStringExtra("alamat"));
            edemail.setText(data.getStringExtra("email"));
            edgelar.setText(data.getStringExtra("gelar"));
            ednidnlama.setText(data.getStringExtra("nidn"));
        }

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_dsn(
                        ednama.getText().toString(),
                        ednidn.getText().toString(),
                        edalamat.getText().toString(),
                        edemail.getText().toString(),
                        edgelar.getText().toString(),
                        "Bebas",
                        "72180201",
                        ednidnlama.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DosenUpdateActivity.this, MainDsnActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenUpdateActivity.this, "Data Tidak Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}