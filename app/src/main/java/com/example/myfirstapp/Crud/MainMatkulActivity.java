package com.example.myfirstapp.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myfirstapp.R;

public class MainMatkulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        //variable
        Button btnGetMt = (Button)findViewById(R.id.buttonGetMatkul);
        Button btnAddMt = (Button)findViewById(R.id.buttonAddMatkul);
        //Button btnUpdateMt = (Button)findViewById(R.id.buttonUpdateMatkul);
        Button btnDeleteMt = (Button)findViewById(R.id.buttonDeleteMatkul);

        //action

        btnGetMt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainMatkulActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnAddMt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainMatkulActivity.this, MatkulAddActivity.class);
                startActivity(intent);
            }
        });

        /*btnUpdateMt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainMatkulActivity.this, MatkulUpdateActivity.class);
                startActivity(intent);
            }
        });*/

        btnDeleteMt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainMatkulActivity.this, MatkulDeleteActivity.class);
                startActivity(intent);
            }
        });










    }
}