package com.example.myfirstapp.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myfirstapp.Login.LoginActivity;
import com.example.myfirstapp.R;

public class UtamaActivity extends AppCompatActivity {
    SharedPreferences session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utama);

        //variable
        Button DataMhs = (Button)findViewById(R.id.buttonDataMhs);
        Button DataDsn = (Button)findViewById(R.id.buttonDataDsn);
        Button DataMatkul = (Button)findViewById(R.id.buttonDataMatkul);
        Button btnLogout = (Button)findViewById(R.id.buttonLogout);

        Toast.makeText(UtamaActivity.this, "DIPILIH", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(UtamaActivity.this);

        if(session.getString("nimnik", "").isEmpty() && session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(UtamaActivity.this, LoginActivity.class));
            return;
        }




        //action


        DataMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( UtamaActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });

        DataDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( UtamaActivity.this, MainDsnActivity.class);
                startActivity(intent);
            }
        });

        DataMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( UtamaActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                SharedPreferences.Editor editor = session.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent intent = new Intent ( UtamaActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });


    }
}