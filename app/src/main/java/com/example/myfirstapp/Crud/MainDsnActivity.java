package com.example.myfirstapp.Crud;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myfirstapp.R;

public class MainDsnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dsn);

        //variable
        Button btnGet = (Button)findViewById(R.id.buttonGetDsn);
        Button btnAdd = (Button)findViewById(R.id.buttonAddDsn);
        Button btnDelete = (Button)findViewById(R.id.buttonDeleteDsn);
        Button btnUpdate = (Button)findViewById(R.id.buttonUpdateDsn);

        //action

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainDsnActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainDsnActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainDsnActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent ( MainDsnActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });

    }
}