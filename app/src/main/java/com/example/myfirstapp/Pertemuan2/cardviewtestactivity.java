package com.example.myfirstapp.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.myfirstapp.Adapter.MahasiswaRecyclerAdapter;
import com.example.myfirstapp.Model.Mahasiswa;
import com.example.myfirstapp.R;

import java.util.ArrayList;
import java.util.List;

public class cardviewtestactivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardviewtestactivity);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Deden Prasetio","72180202","0812929840321");
        Mahasiswa m2 = new Mahasiswa("Vicky Prasetio","72180203","0812929840332");
        Mahasiswa m3 = new Mahasiswa("Judas Prasetio","72180204","0812929843218");
        Mahasiswa m4 = new Mahasiswa("Alex Prasetio","72180205","081292984065");
        Mahasiswa m5 = new Mahasiswa("Roy Prasetio","72180206","08129298354");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(cardviewtestactivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(cardviewtestactivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);

    }
}