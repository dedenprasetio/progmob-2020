package com.example.myfirstapp.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myfirstapp.R;

public class PrefActivity extends AppCompatActivity {
    String isiLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        //membaca pref isilogin apakah true atau false
        isiLogin = pref.getString("isiLogin","0");
        if(isiLogin.equals("1")){
            btnPref.setText("Logout");
        }else {
            btnPref.setText("Login");
        }

        //pengisian pref
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isiLogin = pref.getString("isiLogin","0");
                if(isiLogin.equals("0")){
                    editor.putString("isiLogin", "1");
                    btnPref.setText("Logout");
                }else{
                    editor.putString("isiLogin", "0");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });


    }
}