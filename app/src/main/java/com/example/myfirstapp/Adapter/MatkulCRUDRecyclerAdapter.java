package com.example.myfirstapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.Crud.DosenUpdateActivity;
import com.example.myfirstapp.Crud.MatkulUpdateActivity;
import com.example.myfirstapp.Model.Dosen;
import com.example.myfirstapp.Model.Matkul;
import com.example.myfirstapp.R;

import java.util.ArrayList;
import java.util.List;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> matkulList;

    public MatkulCRUDRecyclerAdapter(Context context){
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_matkul,parent,false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul mt = matkulList.get(position);

        holder.tvKodeMatkul.setText(mt.getKode());
        holder.tvNamaMatkul.setText(mt.getNama());
        holder.tvHari.setText(mt.getHari());
        holder.tvSesi.setText(mt.getSesi());
        holder.tvSks.setText(mt.getSks());
        holder.mt = mt;


        //holder.tvNoTelp.setText(m.getNotelp());


    }


    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvKodeMatkul, tvNamaMatkul, tvHari, tvSesi, tvSks;
        private RecyclerView rvMatkulGetAll;
        Matkul mt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKodeMatkul = itemView.findViewById(R.id.tvKodeMatkul);
            tvNamaMatkul = itemView.findViewById(R.id.tvNamaMatkul);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesi);
            tvSks = itemView.findViewById(R.id.tvSks);
            rvMatkulGetAll = itemView.findViewById(R.id.rvMatkulGetAll);


            //tvNoTelp = itemView.findViewById(R.id.tvNoTelp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), MatkulUpdateActivity.class);
                    intent.putExtra("kode",mt.getKode());
                    intent.putExtra("nama",mt.getNama());
                    intent.putExtra("hari",mt.getHari());
                    intent.putExtra("sesi",mt.getSesi());
                    intent.putExtra("sks",mt.getSks());

                    //intent.putExtra("alamat",m.getAlamat());
                    //intent.putExtra("email",m.getEmail());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
